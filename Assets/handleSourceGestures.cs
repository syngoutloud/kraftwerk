﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;

public class handleSourceGestures : MonoBehaviour
{

    public TapGesture doubleTap;
    bool isFocused = false;
    Vector3 startingScale;
    handleState HandleState;
    public GameObject colorSphere;
    Vector3 colorSphereInitialScale;
    Vector3 colorSphereFocusedScale;
    public GameObject spaceFloor;
    Vector3 floorInitialScale;
    public GameObject iconIso;
    public GameObject iconTopDown;
    public GameObject coverParent;
    //public GameObject currentCoverCircleCropped;
    //public GameObject currentCoverSquare;


    // Start is called before the first frame update
    void Start()
    {
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
        startingScale = transform.localScale;
        colorSphereFocusedScale = new Vector3(0.31f, 0.31f, 0.32f);
        colorSphereInitialScale = colorSphere.transform.localScale;
        doubleTap = gameObject.GetComponent<TapGesture>();
        doubleTap.Tapped += DoubleTap_Tapped;
        floorInitialScale = spaceFloor.transform.localScale;
    }

    private void DoubleTap_Tapped(object sender, System.EventArgs e)
    {
        Debug.Log("double tappy");
        
        Invoke("focusThisSource", 0.1f);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void focusThisSource()
    {
        isFocused = true;
        GetComponent<SphereCollider>().enabled = false;
        GetComponent<spinGeneric>().enabled = false;
        if (HandleState.viewIsIso == true)
        {
            transform.DORotate(new Vector3(-50, 0, 0), 0.3f);

        }
        else
        {
            transform.DORotate(new Vector3(0, 0, 0), 0.3f);

        }
        transform.DOMove(new Vector3(0, 0, 0), 0.5f).SetEase(Ease.OutBack); 
        transform.DOScale(startingScale * 10, 0.5f).SetEase(Ease.OutBack);
        colorSphere.transform.DOScale(colorSphereFocusedScale, 0.5f);
        HandleState.Invoke("showPlaylist", 0.3f);
        GameObject.Find("sphereBackgroundColor").GetComponent<SpriteRenderer>().DOFade(0, 0.3f);
        spaceFloor.transform.DOScale(new Vector3(0, 0, 0), 0.4f);
        iconIso.SetActive(false);
        iconTopDown.SetActive(false);
        coverParent.transform.DOLocalMove(new Vector3(0, 0.1f, 0.02f), 0.3f);
        HandleState.currentAlbumArtSquare.GetComponent<SpriteRenderer>().DOFade(1, 0.2f);
        HandleState.currentAlbumArtCircle.GetComponent<SpriteRenderer>().DOFade(0, 0.2f);
    }

    public void defocusThisSource()
    {
        isFocused = false;
        GetComponent<SphereCollider>().enabled = true;
        GetComponent<spinGeneric>().enabled = true;
        transform.DORotate(new Vector3(0, 0, 0), 0.5f);
        colorSphere.transform.DOScale(colorSphereInitialScale, 0.5f);
        transform.DOScale(startingScale, 0.3f).SetEase(Ease.InOutQuad);
        HandleState.hidePlaylist();
        GameObject.Find("sphereBackgroundColor").GetComponent<SpriteRenderer>().DOFade(1, 0.3f);
        GameObject.Find("spaceFloor").SetActive(true);
        spaceFloor.transform.DOScale(floorInitialScale, 0.3f);
        iconIso.SetActive(true);
        iconTopDown.SetActive(true);
        coverParent.transform.DOLocalMove(new Vector3(0, 0, 0), 0.3f);
        HandleState.currentAlbumArtSquare.GetComponent<SpriteRenderer>().DOFade(0, 0.3f).SetDelay(0.2f);
        HandleState.currentAlbumArtCircle.GetComponent<SpriteRenderer>().DOFade(1, 0.3f).SetDelay(0.2f);

    }


}
