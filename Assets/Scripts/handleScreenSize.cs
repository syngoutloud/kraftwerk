﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class handleScreenSize : MonoBehaviour
{

    GameObject sceneCamObj;
    Camera cam;
    
    public GameObject iconClose;
    public GameObject iconViewTopDown;
    public GameObject iconViewIso;
    public GameObject iconBack;

    float screenWidth;
    float screenHeight;

    float screenMargin;

    float scaleIpad;
    float scaleIphone;

    Vector3 iconScale;

    public GameObject targetDrawer;
    public GameObject jungleSpace;
    public GameObject targetSpace;
    

    void Start()
    {
        scaleIpad = 0.18f;
        scaleIphone = 0.25f;

        cam = Camera.main;

        //Debug.Log(cam.pixelRect);
        screenWidth = cam.pixelRect.width;
        screenHeight = cam.pixelRect.height;
        
        float aspectRatio = screenHeight / screenWidth;

        print(aspectRatio);

        if (SystemInfo.deviceModel.Contains("iPad") || aspectRatio < 1.5f) //quick aspect ratio in-editor hack
        {
            //print("1");
            iconScale = new Vector3(scaleIpad, scaleIpad, scaleIpad);
            screenMargin = screenHeight / 25;
            cam.orthographicSize = 6.5f;
            targetDrawer.transform.position = new Vector3(0, -7.4f, 0);
            jungleSpace.transform.position = targetSpace.transform.position;

        }
        else
        {
           // print("2");
            iconScale = new Vector3(scaleIphone, scaleIphone, scaleIphone);
            screenMargin = screenHeight / 20;
            cam.orthographicSize = 8.2f;
            targetDrawer.transform.position = new Vector3(0, -9, 0);
            jungleSpace.transform.position = targetSpace.transform.position;
        }

        positionCameraLockedUI();

    }

    void positionCameraLockedUI()
    {
        print("called positionCameraLockedUI");

        float zPos = 8;

        iconClose.transform.localScale = iconScale * 0.95f;
        iconViewIso.transform.localScale = iconScale;
        iconViewTopDown.transform.localScale = iconScale;
        iconBack.transform.localScale = iconScale * 0.95f;
       
        iconClose.transform.position = cam.ScreenToWorldPoint(new Vector3(screenWidth - screenMargin, screenHeight - screenMargin, zPos));
        iconBack.transform.position = cam.ScreenToWorldPoint(new Vector3(screenMargin*0.6f, screenHeight - screenMargin, zPos));

        iconViewTopDown.transform.position = cam.ScreenToWorldPoint(new Vector3(screenMargin, screenMargin, zPos));
        iconViewIso.transform.position = cam.ScreenToWorldPoint(new Vector3(screenWidth - screenMargin, screenMargin, zPos));
    }

    // Update is called once per frame
    void Update()
    {

    }

}

