﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TouchScript.Gestures.TransformGestures;

public class handleState : MonoBehaviour
{

    public bool soundScapeIsInDrawer = false;
    public GameObject targetSpace;
    public GameObject targetDrawer;
    public GameObject jungleSpace;
    public GameObject closeX;
    public SphereCollider jungleSpaceSphereCollider;
    public TransformGesture jungleSpaceTransformGesture;
    public AudioSource jungleSpaceBackgroundAudio;
    public AudioSource raincloudAudio;
    public GameObject sphereBackgroundColor;
    public Color skyColorDay;
    public Color skyColorNight;
    public Color skyColorOff;
    public Color cameraBkgColorOff;
    public Color cameraBkgColorDay;
    public GameObject drawerBackgroundCircle;
    public GameObject rainCloud;
    //public GameObject rainParticles;
    Vector3 rainCloudStartingPos;
    public GameObject timeControl;
    //public AudioSource audioLoopCrickets;
    //public AudioSource audioLoopBirds;
    //public AudioSource audioLoopBatFlapping;
    public GameObject skyOverlay;
    public GameObject timeControlRotator;
    public GameObject timeControlHiddenDragHandle;
    public GameObject timeControlVisibleDragHandle;
    public Light mainLight;
    public GameObject mainLightParent;
    public bool viewIsIso = true;
    public GameObject camParent;
    public GameObject iconIso;
    public GameObject iconTopDown;
    public GameObject iconBack;
    public GameObject playlist;
    public AudioSource currentTrack;
    public AudioClip clipAutobahn;
    public AudioClip clipModel;
    public AudioClip clipRadio;
    public GameObject selectionHighlight;
    public GameObject currentAlbumArtSquare;
    public GameObject currentAlbumArtCircle;
    public GameObject albumArtBlueSquare;
    public GameObject albumArtRedSquare;
    public GameObject albumArtYellowSquare;
    public GameObject albumArtBlueCircle;
    public GameObject albumArtRedCircle;
    public GameObject albumArtYellowCircle;
    public GameObject colorSphere;
    public Material sphereBlue;
    public Material sphereRed;
    public Material sphereYellow;

    public int currentTrackNum;
    //0 auto, 1 model, 2 radio




    // Start is called before the first frame update
    void Start()
    {
        currentTrackNum = 0;
        Application.targetFrameRate = 60;
        soundScapeIsInDrawer = false;
        //rainCloudStartingPos = rainCloud.transform.localPosition;
        //setJungleDraggableState();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playSelectedTrack()
    {
        currentTrack.Stop();

        switch (currentTrackNum)
        {
            case 0:
                currentTrack.clip = clipAutobahn;
                selectionHighlight.transform.DOLocalMoveY(-3.45f, 0.1f);

                albumArtBlueSquare.SetActive(true);
                albumArtRedSquare.SetActive(false);
                albumArtYellowSquare.SetActive(false);

                albumArtBlueCircle.SetActive(true);
                albumArtRedCircle.SetActive(false);
                albumArtYellowCircle.SetActive(false);

                currentAlbumArtCircle = albumArtBlueCircle;
                currentAlbumArtSquare = albumArtBlueSquare;

                colorSphere.GetComponent<MeshRenderer>().material = sphereBlue;

                break;
            case 1:
                currentTrack.clip = clipModel;
                selectionHighlight.transform.DOLocalMoveY(-4.45f, 0.1f);
                
                albumArtBlueSquare.SetActive(false);
                albumArtRedSquare.SetActive(true);
                albumArtYellowSquare.SetActive(false);

                albumArtBlueCircle.SetActive(false);
                albumArtRedCircle.SetActive(true);
                albumArtYellowCircle.SetActive(false);

                currentAlbumArtCircle = albumArtRedCircle;
                currentAlbumArtSquare = albumArtRedSquare;

                colorSphere.GetComponent<MeshRenderer>().material = sphereRed;

                break;
            case 2:
                currentTrack.clip = clipRadio;
                selectionHighlight.transform.DOLocalMoveY(-5.45f, 0.1f);
                
                albumArtBlueSquare.SetActive(false);
                albumArtRedSquare.SetActive(false);
                albumArtYellowSquare.SetActive(true);

                albumArtBlueCircle.SetActive(false);
                albumArtRedCircle.SetActive(false);
                albumArtYellowCircle.SetActive(true);

                currentAlbumArtCircle = albumArtYellowCircle;
                currentAlbumArtSquare = albumArtYellowSquare;

                colorSphere.GetComponent<MeshRenderer>().material = sphereYellow;


                break;
            default:
                break;
        }

        currentTrack.Play();
    }

    public void showPlaylist()
    {
        closeX.SetActive(true);
        iconBack.SetActive(true);
        playlist.SetActive(true);

    }

    public void hidePlaylist()
    {
        closeX.SetActive(false);
        iconBack.SetActive(false);
        playlist.SetActive(false);
    }

    public void setCamView()
    {
        if (viewIsIso == true)
        {
            //camParent.transform.eulerAngles = new Vector3(0, 0, 0);
            camParent.transform.DORotate(new Vector3(0, 0, 0), 0.4f);
        }
        else
        {
            //camParent.transform.eulerAngles = new Vector3(50, 0, 0);
            camParent.transform.DORotate(new Vector3(50, 0, 0), 0.4f);
        }
    }

    //public void returnJungleToDrawer()
    //{
    //    jungleSpace.transform.DOMove(targetDrawer.transform.position, 0.4f).SetEase(Ease.OutBack);
    //    jungleSpace.transform.DOScale(new Vector3(0.2f, 0.2f, 0.2f), 0.4f).SetEase(Ease.OutBack);
    //    jungleSpace.GetComponent<SphereCollider>().enabled = true;
    //    jungleSpace.GetComponent<TransformGesture>().enabled = true;
    //    closeX.SetActive(false);
    //    soundScapeIsInDrawer = true;
    //    setJungleDraggableState();
    //    skyOverlay.GetComponent<SpriteRenderer>().DOFade(0, 1);

    //    timeControlRotator.transform.eulerAngles = new Vector3(40, 0, -0);
    //    timeControlHiddenDragHandle.transform.position = timeControlVisibleDragHandle.transform.position;
        
    //    mainLightParent.transform.eulerAngles = new Vector3(0, 0, 0);

    //    viewIsIso = true;
    //    setCamView();
    //    GameObject.Find("iconTopDownHighlight").GetComponent<SpriteRenderer>().DOFade(0, 0.2f);
    //    GameObject.Find("iconIsoHighlight").GetComponent<SpriteRenderer>().DOFade(1, 0.2f);

    //    iconIso.SetActive(false);
    //    iconTopDown.SetActive(false);

        


    //}

    //public void setJungleDraggableState()
    //{
    //    if (soundScapeIsInDrawer == true)
    //    {
    //        jungleSpaceSphereCollider.enabled = true;
    //        jungleSpaceTransformGesture.enabled = true;
    //        closeX.SetActive(false);
    //       // sphereBackgroundColor.GetComponent<SpriteRenderer>().DOColor(skyColorOff, 0.5f);
    //        //Camera.main.DOColor(cameraBkgColorOff, 0.5f);
    //        //jungleSpaceBackgroundAudio.DOFade(0, 1);
    //        //audioLoopBirds.DOFade(0, 1);
    //        //audioLoopCrickets.DOFade(0, 1);
    //        raincloudAudio.DOFade(0f, 1);
    //        drawerBackgroundCircle.GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
    //        rainCloud.transform.DOLocalMove(rainCloudStartingPos, 0.5f);
    //        //rainParticles.SetActive(false);
    //        timeControl.transform.DOScale(new Vector3(0f, 0f, 0f), 0.25f);
    //        mainLight.DOIntensity(1, 0.4f);
    //        //mainLight.DOShadowStrength(1, 0.4f);
    //        //audioLoopBatFlapping.DOFade(0, 1);
    //        iconIso.SetActive(false);
    //        iconTopDown.SetActive(false);

    //    }
    //    else
    //    {
    //        jungleSpaceSphereCollider.enabled = false;
    //        jungleSpaceTransformGesture.enabled = false;
    //        closeX.SetActive(true);
    //       // Camera.main.DOColor(cameraBkgColorDay, 0.5f);
    //       // sphereBackgroundColor.GetComponent<SpriteRenderer>().DOColor(skyColorDay, 0.5f);
    //        //jungleSpaceBackgroundAudio.DOFade(0.165f, 1);
    //        raincloudAudio.DOFade(0.14f, 1);
    //        drawerBackgroundCircle.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
    //        //rainParticles.SetActive(true);
    //        timeControl.transform.DOScale(new Vector3(1, 1, 1), 0.4f).SetEase(Ease.OutBack).SetDelay(0.3f);
            
    //        //audioLoopBatFlapping.DOFade(0.45f, 1);
    //        iconIso.SetActive(true);
    //        iconTopDown.SetActive(true);
    //    }
    //}
}
