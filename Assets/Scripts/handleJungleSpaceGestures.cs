﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;

public class handleJungleSpaceGestures : MonoBehaviour
{
    public GameObject drawerSpot;
    public GameObject spaceCenter;
    float snapThresholdDistance = 5f;
    float distanceToDrawerSpot;
    float distanceFromSpaceCenter;
    handleState HandleState;
    

    // Start is called before the first frame update
    void Start()
    {
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;
    }

    // Update is called once per frame
    void Update()
    {
        getDistanceFromSpaceCenter();
    }

    void getDistanceToDrawerSpot()
    {
        distanceToDrawerSpot = Vector3.Distance(transform.position, drawerSpot.transform.position);
       // print(distanceToDrawerSpot);
    }

    void getDistanceFromSpaceCenter()
    {
        distanceFromSpaceCenter = Vector3.Distance(transform.position, spaceCenter.transform.position);
       // print(distanceFromSpaceCenter);
    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("puck released!");

            if (distanceFromSpaceCenter > snapThresholdDistance)
            {
                transform.DOMove(drawerSpot.transform.position, 0.4f).SetEase(Ease.OutBack);
                HandleState.soundScapeIsInDrawer = true;
                //HandleState.setJungleDraggableState();
            }
            else
            {
                transform.DOMove(spaceCenter.transform.position, 0.4f).SetEase(Ease.OutBack);
                transform.DOScale(new Vector3(1, 1, 1), 0.4f).SetEase(Ease.OutBack);
                HandleState.soundScapeIsInDrawer = false;
                //HandleState.setJungleDraggableState();

            }


        }

    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("puck pressed!");


            //if (gesture.gameObject.tag == "buttonCloseX") // go home
            //{

            //}


        }

    }
}
